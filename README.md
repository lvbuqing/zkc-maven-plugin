# 项目简介
zkc-maven-plugin 自定义maven插件，基于自定义maven插件和自定义类加载器实现分应用模块打包。

#### 插件使用

![use_example.png](image/use_example.png)

## 参数说明
```
<configuration>
   <applicationModuleJson>moduleManifest.json</applicationModuleJson>
</configuration>
```
- applicationModuleJson: 本次需要打包的应用所包含的Module Json配置，支持多个应用打包“逗号”隔开

## moduleManifest.json 配置示例
```
[
  {
    "moduleId": "user",
    "moduleName": "用户模块"
  }

]

```
# 插件设计思路：
![use_example.png](image/maven_sj.png)

# 插件运行结果：
```
[INFO] --- pre-maven-plugin:1.0-SNAPSHOT:jwfRepackage (default) @ web ---
自定义分模块打包插件开始执行：[1,608,561,494,098]
排除非应用模块类：[BOOT-INF/classes/com/zkc/web/controller/ProductController.class]
排除非应用模块类：[BOOT-INF/classes/com/zkc/web/controller/RoleController.class]
自定义分模块打包插件结束执行，供排除[2]个非应用模块Class，耗时：[160]毫秒
[INFO] ------------------------------------------------------------------------
```
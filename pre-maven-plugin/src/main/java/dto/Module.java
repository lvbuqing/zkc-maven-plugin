package dto;

import lombok.Data;

/**
 * @author zkc
 * @version 1.0.0
 * @since 2020-12-21
 */
@Data
public class Module {

    private String moduleId;
    private String moduleName;

}

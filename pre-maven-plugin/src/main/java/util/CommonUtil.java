package util;

import java.lang.reflect.Array;
import java.util.Collection;

/**
 * @author zkc
 * @version 1.0.0
 * @since 2020-12-21
 */
public class CommonUtil {

    public static boolean isCollectionEmpty(Collection<?> collection){
        return collection == null || collection.isEmpty();
    }

    public static boolean isArrayEmpty(Object[] array){
        return getLength(array) == 0;
    }

    public static int getLength(Object array) {
        return array == null ? 0 : Array.getLength(array);
    }


}

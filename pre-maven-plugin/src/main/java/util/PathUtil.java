package util;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.List;

/**
 * @author zkc
 * @version 1.0.0
 * @since 2020-12-21
 */
public class PathUtil {

    public static final String JAR = ".jar";
    public static final String CLASS = ".class";
    private static final String CLASSES = "classes";
    public static final String MODULE_CLASS = "com.zkc.web.annotation.Module";
    public static final String MODULE_METHOD = "moduleId";
    public static final String COMMON_CONTROLLER = "com.zkc.web.controller";
    public static final String COMMON_ANNOTATION = "com.zkc.web.annotation";

    public static String buildModuleJsonPath(List<String> compilePath, String moduleJson, String applicationFolderName) {
        return getCompilePathFirst(compilePath)
                + File.separator
                + applicationFolderName
                + File.separator
                + moduleJson;
    }

    public static String buildJarPath(List<String> compilePath) {
        return getCompilePathFirst(compilePath)
                .replace(CLASSES, StringUtils.EMPTY);
    }

    public static String buildJarName(String controllerArtifactId, String projectVersion) {
        return controllerArtifactId + "-" + projectVersion + JAR;
    }

    private static String getCompilePathFirst(List<String> compilePath) {
        if (CommonUtil.isCollectionEmpty(compilePath)) {
            return StringUtils.EMPTY;
        }
        return compilePath.get(0);
    }

    public static String getMavenPluginPath(String path) {
        if (StringUtils.isBlank(path)) {
            return StringUtils.EMPTY;
        }
        String mavenPluginPath;
        if (isWindows(path)) {
            mavenPluginPath = path.replace("file:/", StringUtils.EMPTY).replace("file:\\", StringUtils.EMPTY);
        } else {
            mavenPluginPath = path.replace("file:", StringUtils.EMPTY);
        }
        return mavenPluginPath.substring(0, mavenPluginPath.lastIndexOf(".jar") + 4);
    }

    private static boolean isWindows(String path) {
        return path.split(":/").length == 3 || path.split("\\:").length == 3;
    }

}

package util;

import org.apache.commons.io.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;

/**
 * @author zkc
 * @version 1.0.0
 * @since 2020-12-21
 */
public class JarUtil {

    public static void delete(String jarName, Set<String> deletes) throws Exception {
        // 先备份原始jar包
        File oriFile = new File(jarName);
        if (!oriFile.exists()) {
            System.out.println(MessageFormat.format("排除类时，原始jar包不存在[{0}]", jarName));
            return;
        }
        // 以时间戳重命名备份文件名
        String bakJarName = jarName.substring(0, jarName.length() - 3) + System.currentTimeMillis() + PathUtil.JAR;
        File bakFile = new File(bakJarName);
        // 备份原始jar包
        FileUtils.copyFile(oriFile, bakFile);
        // 排除deletes中的class，重新打包新jar包文件
        JarFile bakJarFile = new JarFile(bakJarName);
        JarOutputStream jos = new JarOutputStream(new FileOutputStream(jarName));
        Enumeration<JarEntry> entries = bakJarFile.entries();
        while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();
            String name = entry.getName().replace("BOOT-INF/classes/", "").replace("\\", ".").replace("/", ".").replace(".class", "");
            if (!deletes.contains(name)) {
                InputStream inputStream = bakJarFile.getInputStream(entry);
                jos.putNextEntry(entry);
                byte[] bytes = readStream(inputStream);
                jos.write(bytes, 0, bytes.length);
            } else {
                System.out.println(MessageFormat.format("排除非应用模块类：[{0}]", entry.getName()));
            }
        }
        // 关闭流
        jos.flush();
        jos.finish();
        jos.close();
        bakJarFile.close();
    }

    private static byte[] readStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outSteam = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = -1;
        while ((len = inStream.read(buffer)) != -1) {
            outSteam.write(buffer, 0, len);
        }
        outSteam.close();
        inStream.close();
        return outSteam.toByteArray();
    }

}

package util;

import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * @author zkc
 * @version 1.0.0
 * @since 2020-12-21
 */
public class ModuleClassLoader extends ClassLoader {

    private final static Map<String, byte[]> MODULE_CLASS_MAP = new ConcurrentHashMap<>();

    public ModuleClassLoader(String moduleJarPath) {
        try {
            readJar(moduleJarPath);
        } catch (IOException e) {
            System.out.println(MessageFormat.format("读取jar包异常：地址[{0}]", moduleJarPath));
            e.printStackTrace();
        }
    }

    public static void addClass(String className, byte[] byteCode) {
        if (!MODULE_CLASS_MAP.containsKey(className)) {
            MODULE_CLASS_MAP.put(className, byteCode);
        }
    }

    /**
     * 遵守双亲委托规则
     *
     * @param name 类全路径
     * @return 返回类字节码对象
     */
    @Override
    protected Class<?> findClass(String name) {
        try {
            byte[] result = getClass(name);
            if (result != null) {
                return defineClass(name, result, 0, result.length);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] getClass(String className) {
        return MODULE_CLASS_MAP.getOrDefault(className, null);
    }

    private void readJar(String moduleJarPath) throws IOException {
        readJarFile(moduleJarPath);
    }

    private void readJarFile(String moduleJarPath) throws IOException {
        File file = new File(moduleJarPath);
        if (!file.exists()) {
            throw new FileNotFoundException();
        }
        JarFile jarFile = new JarFile(file);
        Enumeration<JarEntry> enumeration = jarFile.entries();
        while (enumeration.hasMoreElements()) {
            JarEntry jarEntry = enumeration.nextElement();
            String name = jarEntry.getName().replace("BOOT-INF/classes/", "").replace("\\", ".")
                    .replace("/", ".");
            if (isRead(name)) {
                String className = name.replace(".class", "");
                try (InputStream input = jarFile.getInputStream(jarEntry); ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                    int bufferSize = 1024;
                    byte[] buffer = new byte[bufferSize];
                    int bytesNumRead;
                    while ((bytesNumRead = input.read(buffer)) != -1) {
                        baos.write(buffer, 0, bytesNumRead);
                    }
                    addClass(className, baos.toByteArray());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean isRead(String name) {
        return StringUtils.isNotBlank(name) && name.endsWith(PathUtil.CLASS)
                && (name.startsWith(PathUtil.COMMON_CONTROLLER) || name.startsWith(PathUtil.COMMON_ANNOTATION));
    }

    public static Map<String, byte[]> getModuleClassMap() {
        return MODULE_CLASS_MAP;
    }
}


package mojo;

import com.alibaba.fastjson.JSON;
import dto.Module;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.*;
import util.*;

/**
 * @author zkc
 * @version 1.0.0
 * @since 2020-12-21
 */
@Mojo(name = "jwfRepackage", defaultPhase = LifecyclePhase.PACKAGE)
@Data
public class ZkcRepackageMojo extends AbstractMojo {
    /**
     * 应用的模块json配置文件，支持“，”逗号分隔多个应用
     */
    @Parameter(required = true)
    private String applicationModuleJson;
    /**
     * 配置应用模块json所在文件夹名称，默认application
     */
    @Parameter
    private String applicationFolderName = "application";
    /**
     * maven编译时classes路径，只读，不需要手动配置
     */
    @Parameter(defaultValue = "${project.compileClasspathElements}", readonly = true, required = true)
    private List<String> compilePath;
    /**
     * maven项目中版本号，只读，不需要手动配置
     */
    @Parameter(defaultValue = "${project.version}", readonly = true, required = true)
    private String projectVersion;
    /**
     * maven项目中artifactId，只读，不需要手动配置
     */
    @Parameter(defaultValue = "${project.artifactId}", readonly = true, required = true)
    private String artifactId;


    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {

        long startTime = System.currentTimeMillis();
        System.out.println(MessageFormat.format("自定义分模块打包插件开始执行：[{0}]", startTime));

        // 解析应用模块json配置文件
        List<Module> moduleList = analysisApplicationModuleJson(StringUtils.split(applicationModuleJson, ","));
        if (CommonUtil.isCollectionEmpty(moduleList)) {
            System.out.println(MessageFormat.format("应用模块json解析为空：maven编译地址[{0}]，应用模块json地址[{1}]", String.join(",", compilePath), applicationModuleJson));
            return;
        }

        // 自定义类加载器，加载原始jar包,过滤出非应用的模块Controller类
        String moduleJarPath = PathUtil.buildJarPath(compilePath) + PathUtil.buildJarName(artifactId, projectVersion);
        Set<String> excludeClasses = getExcludeClasses(moduleJarPath, moduleList);
        if (CommonUtil.isCollectionEmpty(excludeClasses)) {
            long endTime = System.currentTimeMillis();
            System.out.println(MessageFormat.format("自定义分模块打包插件结束执行，没有需要排除的模块Class，耗时：[{0}]毫秒", endTime - startTime));
            return;
        }

        // 删除无用的应用模块Controller类，重新打包
        try {
            JarUtil.delete(moduleJarPath, excludeClasses);
        } catch (Exception e) {
            System.out.println(MessageFormat.format("删除无用应用模块Controller类，重新打包失败：[{0}],[{1}]", moduleJarPath, excludeClasses));
            e.printStackTrace();
        }

        long endTime = System.currentTimeMillis();
        System.out.println(MessageFormat.format("自定义分模块打包插件结束执行，供排除[{0}]个非应用模块Class，耗时：[{1}]毫秒", excludeClasses.size(), endTime - startTime));

    }

    /**
     * 解析应用模块json配置文件
     *
     * @param moduleJsons 应用模块json配置地址
     * @return 返回解析后的模块集合
     */
    private List<Module> analysisApplicationModuleJson(String[] moduleJsons) {
        if (CommonUtil.isArrayEmpty(moduleJsons) || CommonUtil.isCollectionEmpty(compilePath)) {
            return null;
        }
        List<Module> moduleList = new ArrayList<>();
        Arrays.stream(moduleJsons).forEach(moduleJson -> {
            String moduleJsonPath = PathUtil.buildModuleJsonPath(compilePath, moduleJson, applicationFolderName);
            File jsonFile = new File(moduleJsonPath);
            if (!jsonFile.exists()) {
                System.out.println(MessageFormat.format("应用模块json文件未找到：[{0}]", moduleJsonPath));
            }
            try {
                List<Module> moduleListTemp = JSON.parseArray(new String(Files.readAllBytes(jsonFile.toPath())),
                        Module.class);
                moduleList.addAll(moduleListTemp);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        return moduleList;
    }

    /**
     * 自定义类加载器，加载原始jar包,过滤出非应用的模块Controller类
     *
     * @param moduleJarPath 原始jar包路径
     * @param moduleList    应用模块类集合
     * @return 返回需要过滤的模块类
     */
    @SuppressWarnings("unchecked")
    private Set<String> getExcludeClasses(String moduleJarPath, List<Module> moduleList) {
        Set<String> excludeClasses = new HashSet<>();
        ModuleClassLoader moduleClassLoader = new ModuleClassLoader(moduleJarPath);
        ModuleClassLoader.getModuleClassMap().forEach((className, bytes) -> {
            // 解析@ModuleController注解
            try {
                Class clazz = moduleClassLoader.loadClass(className);
                Annotation[] annotations = clazz.getAnnotationsByType(moduleClassLoader.loadClass(PathUtil.MODULE_CLASS));
                for (Annotation annotation : annotations) {
                    Class aClass = annotation.getClass();
                    try {
                        Method method = aClass.getMethod(PathUtil.MODULE_METHOD);
                        String value = (String) method.invoke(annotation);
                        boolean moduleClassFlag = moduleList.stream().anyMatch(module -> Objects.equals(module.getModuleId(), value));
                        if (!moduleClassFlag) {
                            excludeClasses.add(className);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (ClassNotFoundException e) {
                System.out.println(MessageFormat.format("加载[{0}]类报错，因未加载对应jar包，可忽略", className));
            }
        });
        return excludeClasses;
    }

}

package com.zkc.web.controller;

import com.zkc.web.annotation.Module;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zkc
 * @version 1.0.0
 * @since 2020-12-21
 */
@Module(moduleId = "role",moduleName = "角色模块")
@RestController
@RequestMapping("/api")
public class RoleController {

    @GetMapping("/role")
    public String role(){
        return "Hello Role";
    }

}

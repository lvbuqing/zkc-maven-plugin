package com.zkc.web.controller;

import com.zkc.web.annotation.Module;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zkc
 * @version 1.0.0
 * @since 2020-12-21
 */
@Module(moduleId = "user",moduleName = "用户模块")
@RestController
@RequestMapping("/api")
public class UserController {

    @GetMapping("/user")
    public String user(){
        return "Hello User";
    }

}

package com.zkc.web.controller;

import com.zkc.web.annotation.Module;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author zkc
 * @version 1.0.0
 * @since 2020-12-21
 */
@Module(moduleId = "product",moduleName = "商品模块")
@RestController
@RequestMapping("/api")
public class ProductController {

    @GetMapping("/product")
    public String product(){
        return "Hello Product";
    }

}

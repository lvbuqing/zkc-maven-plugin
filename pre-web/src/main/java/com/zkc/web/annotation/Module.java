package com.zkc.web.annotation;

import java.lang.annotation.*;

/**
 * @author zkc
 * @version 1.0.0
 * @since 2020-12-21
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Module {

    String moduleId();

    String moduleName();

}
